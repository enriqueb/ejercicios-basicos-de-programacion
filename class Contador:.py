class Contador:
    
    def __init__(self, num=0):
        self.__numero= num
    #SI devuelves el self, estas devolviendo el contador.
    def get_numero(self):
        return self.__numero
    def set_numero(self, val):
        self.__numero=val
    def incrementar(self, incremento=0):
        self.__numero=self.__numero + incremento 
        return self 
    
    def decrementar(self, decremento=1):
        decrementado=self.__numero + decremento
        return self
    
    def __str__(self):
        return str(self.__numero)

    
    def __eq__(self, other):
        return self.__numero== other.__numero

    
    def __me__(self, other):
        return not self.__eq__(other)



#get te da las tripas de una clase.
#set te hace que puedas cambiarlo

contador= Contador(20)
print(contador.incrementar(1))
print(contador.decrementar(8))
print(contador)
print(contador.incrementar())
contador.set_numero(-1)
print(contador.get_numero())