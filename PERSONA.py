# declaramos la clase persona
class Persona:
    # declaramos el metodo __init__
    def __init__(self, n, e):
        self.nombre=n
        self.edad=e
        if self.edad<0:
            raise exception("edad no puede ser negativa")
            #hay que hacerlo en los testsss
    def get_nombre(self):
        return self.nombre
    
    def set_nombre(self, val):
        self.nombre= val
    # declaramos el metodo str
    def __str__(self):
       return "Nombre " + self.nombre +\
           "\nEdad: " + str(self.edad)
 
#Declaramos la clase empleado
#laclase empleado hereda los atributos y metodos de la clase Persona
class Empleado(Persona):
    def __init__(self,n,e,sueld):
    #llamamos a la clase init del padre
        super().__init__(n, e)
        self.sueldo=sueld
    
    def __str__(self):
        return super().__str__() +\
           "\nSueldo:" + str(self.sueldo)



nomb=input("Ingrese el nombre: ")
ed= int(input("Ingrese la edad:") )
#Cuando un hijo utiliza el mismo codigo que el padre se llama sobreescribir 
p1= Persona(nomb, ed)
print(p1)

sueldo= float(input("Introduce el sueldo:"))
p2= Empleado(nomb, ed, sueldo)
print(p2)
